const isDirectory = source => lstatSync(source).isDirectory()
const getDirectories = source => readdirSync(source)
	.map(name => path.join(source, name))
	.filter(isDirectory)
const sass = require("node-sass")
const merge = require('webpack-merge')
const sassUtils = require("node-sass-utils")(sass)
const sassVars = require("./src/stylesheets/theme")
const { lstatSync, readdirSync } = require('fs')
const path = require('path')

// Plugins
const HtmlWebPackPlugin = require("html-webpack-plugin")
const CopyPlugin = require("copy-webpack-plugin")
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const CompressionPlugin = require('compression-webpack-plugin')

const dev = {
	entry: {
		journey: './demo.js'
	},
	devServer: {
		contentBase: path.join(__dirname, 'public')
	},
	resolve: {
		extensions: [
			'.js', '.hb', '.scss'
		]
	},
	plugins: [
		new HtmlWebPackPlugin({
			template: './public/index.html',
			filename: './index.html'
		})
	]
}

const lib = {
	devtool: 'cheap-source-map',
	entry: {
		'journey': './index.js'
	},
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'js/[name].min.js',
		library: 'Journey',
		libraryTarget: 'var',
	},
	/*plugins: [
		new CompressionPlugin({
			test: /\.js(\?.*)?$/i
		})
	]*/
}

const demo = {
	devtool: 'cheap-source-map',
	entry: {
		'demo': './demo.js'
	},
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'js/demo.min.js',
	},
	plugins: [
		new HtmlWebPackPlugin({
			template: './public/index.html',
			filename: './index.html'
		}),
		new CopyPlugin([
			{ from: 'public', ignore: ['index.html'] },
		]),
		new CompressionPlugin({
			test: /\.js(\?.*)?$/i
		})
	]
}

const base = {
	resolve: {
		extensions: [
			'.js', '.hb', '.scss'
		]
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader'
				}
			},
			{
				test: /\.hb$/,
				use: [
					{
						loader: 'handlebars-loader',
						options: {
							partialDirs: getDirectories(path.resolve(__dirname, './src/components')),
							helperDirs: [path.resolve(__dirname, './src/helpers')],
						}
					},
					'webpack-handlebars-whitespace-loader'
				]
			},
			{
				test: /\.scss$/,
				use: [
					{
						loader: MiniCssExtractPlugin.loader,
						options: {
							//
						}
					},
					'css-loader',
					{
						loader: 'sass-loader',
						options: {
							data: "@import 'core.scss';",
							includePaths: [path.resolve(__dirname, './src/stylesheets')],
							functions: {
								// Pass variables to sass
								"get($keys)": function(keys) {
									let i, result = sassVars

									keys = keys.getValue().split(".")
									for (i = 0; i < keys.length; i++)
										result = result[keys[i]]

									return sassUtils.castToSass(result)
								}
							}
						}
					}
				]
			}
		]
	},
	plugins: [
		new MiniCssExtractPlugin({
			// Options similar to the same options in webpackOptions.output
			// both options are optional
			filename: 'css/[name].min.css',
			chunkFilename: '[id].css',
		})
	]
}

module.exports = mode => {
	if (mode === "production") {
		return [
			merge(base, lib, { mode }),
			merge(base, demo, { mode }),
		]
	}

	return merge(base, dev, { mode })
}
