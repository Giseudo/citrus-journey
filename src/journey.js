import components from './components'
import theme from './stylesheets/theme'
import './stylesheets/core.scss'

// Main component
import template from './journey.hb'
import './journey.scss'

// The default options object
const defaults = {
	active: true,
	baseUrl: './',
	filters: [
		{ label: "Últimos 7 dias", value: 7 },
		{ label: "Últimos 30 dias", value: 30 }
	],
	descriptionLength: 60,
	selectedFilter: 7,
	stressIcons: {
		'-1': 'fas fa-frown-open',
		'0': 'fas fa-meh',
		'1': 'fas fa-smile'
	}
}

export default class Journey {
	constructor(el, data, options) {
		// The DOM element
		this.el = el

		// The API data object
		this.data = JSON.parse(JSON.stringify(data))

		// The components module
		this.components = require('./components')(this)

		// Set options
		this.options = Object.assign({}, defaults, options)

		// Default properties
		this.channels = this.data.channels || []

		// Initialize
		this.init()
	}

	// Initialization
	init() {
		// Subscribe to filter change event
		this.el.addEventListener('filter', event =>
			this.options.selectedFilter = event.detail
		)

		// Updates & renders template
		this.update()

		// Emit init event
		this.emit('init', this)
	}

	// Destroy
	destroy() {
		// Emit destroy event
		this.emit('destroy', this)

		// Destroy components
		this.components.destroy()
	}

	// Open the body
	open() {
		let instance = this.el.querySelector('.cj-journey'),
			body = this.el.querySelector('.cj-journey__body'),
			timeline = this.el.querySelector('.cj-journey__timeline')

		this.options.active = true
		instance.classList.add('is-active')

		// Set the height
		body.style.height = `${timeline.clientHeight}px`

		// Emit open event
		this.emit('open', this)
	}

	// Close the body
	close() {
		let instance = this.el.querySelector('.cj-journey'),
			body = this.el.querySelector('.cj-journey__body')

		this.options.active = false
		instance.classList.remove('is-active')

		// Set the height
		body.style.height = '0px'

		// Emit close event
		this.emit('close', this)
	}

	// Replaces the data object
	set(data) {
		this.data = JSON.parse(JSON.stringify(data))
		this.update()
	}

	// Replaces filter options
	setFilters(filters) {
		this.options.filters = filters
		this.update()
	}

	// Replaces the channels data
	setChannels(channels) {
		this.channels = channels
	}

	// Replaces the interactions data
	setInteractions(interactions = []) {
		this.interactions = this.parseInteractions(interactions)
		this.update()
	}

	// Updates component
	update() {
		this.emit('update', this)

		// Parse & sort interactions
		this.interactions = this.parseInteractions(this.data.interactions)
		this.interactions = this.sortInteractions(this.interactions)

		// Render template passing data
		if (process.env.NODE_ENV != 'testing')
			this.el.innerHTML = template({
				// Scoped data
				active: this.options.active,
				channels: this.channels,
				interactions: this.interactions,
				filters: this.options.filters,
				selectedFilter: this.options.selectedFilter
			}, {
				// Global data
				data: {
					baseUrl: this.options.baseUrl,
					colors: theme.colors
				}
			})

		// Get elements
		let header = this.el.querySelector('.cj-journey__header'),
			instance = this.el.querySelector('.cj-journey')

		// Opens body if it is active
		if (this.options.active)
			this.open()

		// Collapses body when clicking on the header
		header.addEventListener('click', event => {
			if (this.options.active)
				this.close()
			else
				this.open()
		})

		// Initialize components
		this.components.init()
	}

	// Subscribe to event
	on(name, callback = () => {}) {
		// Subscribe to event and pass self reference to the callback
		this.el.addEventListener(name, event => callback(event.detail))
	}

	// Emits event
	emit(name, payload = {}, target = null) {
		let el = target || this.el,
			event = new CustomEvent(name, {
				bubbles: true,
				detail: payload
			})

		// The event is dispatched from target
		el.dispatchEvent(event)
	}

	// Format many interactions
	parseInteractions(interactions = []) {
		return interactions.map((interaction, index) => {
			return this.parseInteraction(interaction)
		})
	}

	// Formats the interaction object
	parseInteraction(interaction) {
		let channel, theme, stressIcon, description

		this.channels.forEach(current =>
			current.id == interaction.channel ? channel = current : null
		)

		// Throw error if channel does not exist
		if (!channel)
			throw new Error(`Channel with id "${interaction.channel}" not found.`)

		// Resolve the theme
		switch(+interaction.stressLevel) {
			case -1:
				theme = 'warn'
				break
			case 0:
				theme = 'accent'
				break
			case 1:
				theme = 'primary'
				break
			default:
				theme = 'default'
				break
		}

		// Strip description tags
		description = interaction.description.replace(/<(?:.|\n)*?>/gm, '')

		// Limit description size
		description = description.substr(0, this.options.descriptionLength)

		// Add dots if there's more hidden text
		if (description.length == this.options.descriptionLength)
			description = `${description}...`

		// The interaction object
		return {
			id: interaction.id,
			date: new Date(interaction.date),
			description: description,
			direction: interaction.direction,
			stressLevel: interaction.stressLevel,
			stressIcon: this.options.stressIcons[interaction.stressLevel],
			source: interaction.source,
      actionDescription: interaction.actionDescription,
      actionIcon: interaction.actionIcon,
      actionTheme: this.getActionTheme(interaction.actionType),
			theme: theme,
			channel: channel
		}
	}

	getActionTheme(type) {
		switch (type) {
			// Orange
      case 'critical':
        return 'secondary'

			// Red
      case 'emergency':
      case 'thanks':
      case 'gift':
				return 'warn'

			// Yellow
      case 'attention':
      case 'survey':
				return 'accent'

			// Green
			case 'apologies':
      case 'levy':
				return 'primary'

			// Gray
			default:
				return 'default'
		}
	}

	// Order interactions by date
	sortInteractions(interactions = []) {
		// Sort function
		let sorted = interactions.sort((a, b) => {
			if (a.date < b.date) return -1
			if (a.date > b.date) return 1
			return 0
		})

		sorted.forEach((interaction, index) => {
			let difference, endColor,
				prev = index > 0 ? interactions[index - 1] : null,
				next = index < interactions.length - 1 ? interactions[index + 1] : null

			// Link interactions
			interaction.prev = prev
			interaction.next = next

			// Set the start color
			interaction.startColor = theme.colors[interaction.theme]

			// Is not the last one
			if (next) {
				// Resolve difference
				difference =
					(interaction.stressLevel == -1 && next.stressLevel == 1) ?
						2 :
					(interaction.stressLevel == 0 && next.stressLevel == 1) ||
					(interaction.stressLevel == -1 && next.stressLevel == 0) ?
						1 :
					(interaction.stressLevel == next.stressLevel) ?
						0 :
					(interaction.stressLevel == 0 && next.stressLevel == -1) ||
					(interaction.stressLevel == 1 && next.stressLevel == 0) ?
						-1 :
					(interaction.stressLevel == 1 && next.stressLevel == -1) ?
						-2 : null

				// Update end color
				endColor = theme.colors[next.theme]

			} else {
				// Update end color
				endColor = interaction.startColor
			}

			// Set difference & endColor
			interaction.difference = difference
			interaction.endColor = endColor
		})

		return sorted
	}

	// Find interaction by id
	getInteraction(id) {
		let interaction

		this.interactions.forEach(current => {
			id == current.id ? interaction = current : null
		})

		return interaction
	}

	// Adds and returns a new interaction
	addInteraction(interaction) {
		this.data.interactions.push(interaction)
		this.emit('addInteraction', interaction)
		this.update()

		return this.getInteraction(interaction.id)
	}
}
