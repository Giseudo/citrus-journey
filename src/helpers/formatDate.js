module.exports = function(value, options) {
	var monthNames = [
		"Janeiro", "Fevereiro", "Março",
		"Abril", "Maio", "Junho", "Julho",
		"Agosto", "Setembro", "Outubro",
		"Novembro", "Dezembro"
	];

	let date = new Date(value),
		day = date.getDate(),
		month = date.getMonth(),
		year = date.getFullYear(),
		hour = date.getHours(),
		minutes = date.getMinutes()

	day = day < 10 ? day = '0' + day : day
	month = month < 10 ? month = '0' + month : month
	hour = hour < 10 ? hour = '0' + hour : hour
	minutes = minutes < 10 ? minutes = '0' + minutes : minutes

	return `${day}/${month}/${year} às ${hour}:${minutes}`;
}
