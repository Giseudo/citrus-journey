const getObject = require('get-object')

module.exports = function(prop, context) {
	return getObject(context, prop)
}
