<div
	class="
		cj-connector
		{{ class }}
	"
>
	<svg class="cj-connector__gradient" width="0" height="0">
			<defs>
				<linearGradient id="gradient--{{ startColor }}-{{ endColor }}" x1="2" y1="2" x2="201" y2="2" gradientUnits="userSpaceOnUse">
					<stop offset="0%" stop-color="{{ startColor }}"/>
					<stop offset="100%" stop-color="{{ endColor }}"/>
				</linearGradient>
			</defs>
	</svg>

	{{#ifCond difference '==' 2 }}
		<svg class="cj-connector__increase-2" width="100%" height="50" viewBox="0 0 160 50" fill="none" xmlns="http://www.w3.org/2000/svg">
			<path
				d="
					M 2,48
					L 45,48
					C 65,48
						100,2
						120,2
					L 158,2
				"
				stroke="url(#gradient--{{ startColor }}-{{ endColor }})"
				stroke-width="4"
				stroke-linecap="round"
			/>
		</svg>
	{{/ifCond }}

	{{#ifCond difference '==' 1 }}
		<svg class="cj-connector__increase-1" width="100%" height="26" viewBox="0 0 160 26" fill="none" xmlns="http://www.w3.org/2000/svg">
			<path
				d="
					M 2,24
					L 65,24
					C 75,24
						85,2
						95,2
					L 158,2
				"
				stroke="url(#gradient--{{ startColor }}-{{ endColor }})"
				stroke-width="4"
				stroke-linecap="round"
			/>
		</svg>
	{{/ifCond }}

	{{#ifCond difference '==' 0 }}
		<svg class="cj-connector__stable" width="100%" height="4" viewBox="0 0 160 4" fill="none" xmlns="http://www.w3.org/2000/svg">
			<path
				d="
					M 2,2
					L 65,2
					C 75,2
						85,2
						85,2
					L 158,2
				"
				stroke="url(#gradient--{{ startColor }}-{{ endColor }})"
				stroke-width="4"
				stroke-linecap="round"
			/>
		</svg>
	{{/ifCond }}

	{{#ifCond difference '==' -1 }}
		<svg class="cj-connector__decrease-1" width="100%" height="26" viewBox="0 0 160 26" fill="none" xmlns="http://www.w3.org/2000/svg">
			<path
				d="
					M 2,2
					L 65,2
					C 75,2
						85,24
						95,24
					L 158,24
				"
				stroke="url(#gradient--{{ startColor }}-{{ endColor }})"
				stroke-width="4"
				stroke-linecap="round"
			/>
		</svg>
	{{/ifCond }}

	{{#ifCond difference '==' -2 }}
		<svg class="cj-connector__decrease-2" width="100%" height="50" viewBox="0 0 160 50" fill="none" xmlns="http://www.w3.org/2000/svg">
			<path
				d="
					M 2,2
					L 45,2
					C 65,2
						100,48
						120,48
					L 158,48
				"
				stroke="url(#gradient--{{ startColor }}-{{ endColor }})"
				stroke-width="4"
				stroke-linecap="round"
			/>
		</svg>
	{{/ifCond }}
</div>
