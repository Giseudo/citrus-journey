{{#ifCond direction '==' 'out' }}
	{{ var "directionIcon" "fas fa-arrow-circle-up" }}
{{else }}
	{{ var "directionIcon" "fas fa-arrow-circle-down" }}
{{/ifCond }}

<div
	class="
		cj-interaction
		{{ class }}
		{{ stressClass }}
	"
	data-id="{{ id }}"
>
	<div class="cj-interaction__header">
		{{> cj-channel
			class="cj-interaction__channel"
			theme=theme
			icon=channel.icon
		}}
		{{> cj-source
			class="cj-interaction__source"
			name=source.label
			image=source.icon
		}}
	</div>

	<div class="cj-interaction__icons">
		{{> cj-icon
			class="cj-interaction__direction"
			size="sm"
			theme="default"
			name=directionIcon
		}}

		{{> cj-icon
			class="cj-interaction__stress"
			size="sm"
			theme=theme
			name=stressIcon
		}}

    {{#if actionIcon }}
      {{> cj-icon
        class="cj-interaction__direction"
        size="sm"
        theme=actionTheme
        name=actionIcon
        tooltip=actionDescription
      }}
		{{/if }}
	</div>

	<div class="cj-interaction__description">
		<span class="cj-text cj-text--body">
			{{{ description }}}
		</span>
	</div>

	<span class="cj-interaction__date cj-text cj-text--body">
		{{ formatDate date }}
	</span>
</div>
