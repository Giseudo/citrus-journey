<button
	class="
		cj-channel
		cj-channel--{{ theme }}
		{{ class }}
	"
>
	{{> cj-icon
		class="cj-channel__icon"
		size="lg"
		theme="default"
		name=icon
	}}
</button>
