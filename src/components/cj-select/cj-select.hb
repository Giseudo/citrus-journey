<div
	class="
		cj-select
		{{ class }}
	"
>
	<label class="cj-select__label cj-text cj-text--body">
		{{ label }}:
	</label>

	<div class="cj-select__wrapper">
		<div class="cj-select__field cj-text cj-text--body">
			{{#each options }}
				{{#ifCond this.value '==' ../selected }}
					{{ this.label }}
				{{/ifCond }}
			{{/each }}
		</div>

		{{> cj-icon
			class="cj-select__arrow"
			name="fas fa-angle-down"
			theme="default"
		}}

		<div class="cj-select__dropdown">
			{{# each options }}
				<button class="cj-select__option cj-text cj-text--body" data-value="{{ this.value }}">
					{{ this.label }}
				</button>
			{{/each }}
		</div>
	</div>
</div>
