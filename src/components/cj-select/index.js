module.exports = journey => ({
	style: require('./style.scss'),

	init() {
		let selects = journey.el.querySelectorAll('.cj-select')
		
		selects.forEach(select => {
			let options = select.querySelectorAll('.cj-select__option'),
				field = select.querySelector('.cj-select__field')

			// Prevent select closing
			select.addEventListener('click', event => {
				select.classList.add('is-active')
				event.stopPropagation()
			})

			// Close select click outside
			journey.el.addEventListener('click', event => {
				select.classList.remove('is-active')
			})

			options.forEach(option => {
				// On option click
				option.addEventListener('click', event => {
					let value = option.dataset.value

					// Update field html
					field.innerHTML = option.innerHTML

					// Close select
					select.classList.remove('is-active')

					// Emit filter event
					journey.emit('filter', value)

					event.preventDefault()
				})
			})
		})
	}
})
