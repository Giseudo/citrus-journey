<div
	class="
		cj-timeline
		reached-end
		{{ class }}
	"
>
	<button class="cj-timeline__scroll cj-timeline__scroll--left">
		{{> cj-icon
			name="fas fa-arrow-left"
			theme="default"
			size="md"
		}}
	</button>

	<button class="cj-timeline__scroll cj-timeline__scroll--right">
		{{> cj-icon
			name="fas fa-arrow-right"
			theme="default"
			size="md"
		}}
	</button>

	<div class="cj-timeline__container">
		{{#each interactions }}
			{{#switch this.stressLevel }}
				{{#case '-1' }}
					{{ var "class" "cj-timeline__entry--bad" }}
				{{/case }}
				{{#case '0' }}
					{{ var "class" "cj-timeline__entry--neutral" }}
				{{/case }}
				{{#case '1' }}
					{{ var "class" "cj-timeline__entry--good" }}
				{{/case }}
			{{/switch }}

			<div class="cj-timeline__entry {{ class }}">
				{{> cj-interaction
					class="cj-timeline__interaction"
					id=this.id
					date=this.date
					description=this.description
					direction=this.direction
					stressIcon=this.stressIcon
					theme=this.theme
					source=this.source
					channel=this.channel
					actionTheme=this.actionTheme
					actionDescription=this.actionDescription
					actionIcon=this.actionIcon
					prev="this.prev"
					next="this.next"
				}}

				{{#if this.next }}
					{{> cj-connector
						class="cj-timeline__connector"
						difference=this.difference
						startColor=this.startColor
						endColor=this.endColor
					}}
				{{/if }}
			</div>
		{{/each }}
	</div>
</div>
