/**
 * http://easings.net/#easeInOutQuart
 *
 * t: current time
 * b: beginning value
 * c: change in value
 * d: duration
 */
function easeInOutQuart(t, b, c, d) {
  if ((t /= d / 2) < 1) return c / 2 * t * t * t * t + b;
  return -c / 2 * ((t -= 2) * t * t * t - 2) + b;
}

/**
 * Returns a number whose value is limited to the given range.
 *
 * Example: limit the output of this computation to between 0 and 255
 * (x * 255).clamp(0, 255)
 *
 * @param {Number} min The lower boundary of the output range
 * @param {Number} max The upper boundary of the output range
 * @returns A number in the range [min, max]
 * @type Number
 */

function clamp(a, b, c){
	return Math.max(b, Math.min(c, a))
}

module.exports = journey => ({
	style: require('./style.scss'),
	scrollLeft: null,

	init() {
		let timelines = journey.el.querySelectorAll('.cj-timeline')

		// Each timeline
		timelines.forEach(timeline => {
			let interactions = timeline.querySelectorAll('.cj-interaction'),
				container = timeline.querySelector('.cj-timeline__container'),
				scrollLeft = journey.el.querySelector('.cj-timeline__scroll--left'),
				scrollRight = journey.el.querySelector('.cj-timeline__scroll--right')

			// Listen to mousewhell event
			container.addEventListener('mousewheel', event => 
				this.onScroll(event, container)
			)

			// Scroll to correct position
			if (this.scrollLeft === null)
        container.scrollLeft = container.scrollWidth - container.clientWidth
      else
      	container.scrollLeft = this.scrollLeft

			// For each interaction
			interactions.forEach(interaction => {
				let channel = interaction.querySelector('.cj-channel'),
					id = interaction.dataset.id

				channel.addEventListener('click', event => {
					let interaction = journey.getInteraction(id)

					journey.emit('selected', interaction)
				})
			})

			// On scroll arrow click
			scrollLeft.addEventListener('click', event => this.scroll(event, 'left', container))
			scrollRight.addEventListener('click', event => this.scroll(event, 'right', container))

			// Toggle arrows on init
      this.toggleArrows(container)
		})
	},

	// Enables horizontal scrolling
	onScroll(event, container) {
		let delta = Math.abs(event.deltaY) >= Math.abs(event.deltaX) ?
			event.deltaY : event.deltaX

		// Horizontaly scroll
		container.scrollLeft += delta
		this.scrollLeft = container.scrollLeft

		this.toggleArrows(container)

		event.preventDefault()
	},

	toggleArrows(container) {
		// If is not at the end add
		if (container.scrollLeft + container.offsetWidth >= container.scrollWidth - 100) {
			container.parentNode.classList.add('reached-end')
			journey.emit('scroll-end', container.scrollLeft)
		} else {
			container.parentNode.classList.remove('reached-end')
		}

		if (container.scrollLeft == 0) {
			container.parentNode.classList.add('reached-start')
			journey.emit('scroll-start', container.scrollLeft)
		} else {
			container.parentNode.classList.remove('reached-start')
		}
	},

	scroll(event, direction, container) {
		let width = container.clientWidth,
			from = container.scrollLeft,
			to = clamp(direction == 'left' ? from - width : from + width, 0, container.scrollWidth),
			duration = 500,
			start = new Date().getTime()

		// Prevent form submition
		event.preventDefault()

		// Emits event
		journey.emit('scroll', direction)

		var timer = setInterval(function() {
			let time = new Date().getTime() - start

			container.scrollLeft = easeInOutQuart(time, from, to - from, duration)

			if (time >= duration)
				clearInterval(timer)

		}, 1000 / 60);

		setTimeout(() => {
			this.toggleArrows(container)
		}, 500)
	}
})
