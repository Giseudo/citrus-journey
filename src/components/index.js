module.exports = journey => {
	const components = {
		'cj-interaction': require('./cj-interaction')(journey),
		'cj-timeline': require('./cj-timeline')(journey),
		'cj-icon': require('./cj-icon')(journey),
		'cj-channel': require('./cj-channel')(journey),
		'cj-source': require('./cj-source')(journey),
		'cj-connector': require('./cj-connector')(journey),
		'cj-select': require('./cj-select')(journey),
		'cj-select': require('./cj-select')(journey),
	}

	return {
		init() {
			for (let name in components) {
				let component = components[name]

				component.init()
			}
		}
	}
}
