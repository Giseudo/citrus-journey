<span
	class="
		cj-icon
		cj-icon--{{ size }}
		cj-icon--{{ theme }}
		{{ class }}
		{{#if tooltip}}cj-icon--tooltip{{/if }}
	"
	data-tooltip="{{ tooltip }}"
>
	<i class="{{ name }}"></i>
</span>
