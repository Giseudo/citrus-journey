<div
	class="
		cj-source
		cj-text cj-text--small
		{{ class }}
	"
	data-tooltip="{{ name }}"
>
	<img class="cj-source__image" src="{{ @baseUrl }}{{ image }}" alt="">
</div>
