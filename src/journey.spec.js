require('ignore-styles').default(['.scss', '.hb'])
const assert = require('chai').assert
const expect = require('chai').expect

const data = require('./data.mock.json')
const Journey = require('./journey').default

describe('Journey', () => {
	let el, journey

	beforeEach(() => {
		el = document.createElement('div')
	})

	describe('Init', () => {
		it('Options should override defaults', () => {
			journey = new Journey(el, {}, {
				baseUrl: '/test/'
			})

			assert.equal(journey.options.baseUrl, '/test/')
		})

		it('Should persist data instead options', () => {
			journey = new Journey(el, data, {
				channels: [],
			})

			assert.equal(journey.channels.length, data.channels.length)
		})
	})

	describe('Interactions', () => {
		beforeEach(() => {
			journey = new Journey(el, data)
		})

		it('Should add new interaction', () => {
			let count = journey.interactions.length

			let interaction = journey.addInteraction({
				id: "id-test",
				date: new Date("2019-05-15 13:33:22"),
				description: "SMS de cobrança. Fatura em aberto",
				stressLevel: 0,
				direction: "out",
				channel: "sms",
				source: {
					id: "lemondesk",
					label: "Lemon Desk",
					icon: "img/lemondesk.png"
				}
			})

			assert.isObject(interaction)
			assert.equal(journey.interactions.length, count + 1)
		})

		it('Should retrieve the interaction with id', () => {
			let interaction = journey.addInteraction({
				id: "id-test",
				date: new Date("2019-05-15 13:33:22"),
				description: "SMS de cobrança. Fatura em aberto",
				stressLevel: 0,
				direction: "out",
				channel: "sms",
				source: {
					id: "lemondesk",
					label: "Lemon Desk",
					icon: "img/lemondesk.png"
				}
			})

			assert.isObject(journey.getInteraction('id-test'))
		})

		it('Should have theme correct theme', () => {
			journey.interactions.forEach(interaction => {
				if (interaction.stressLevel == -1)
					assert.equal(interaction.theme, 'warn')
				if (interaction.stressLevel == 0)
					assert.equal(interaction.theme, 'accent')
				if (interaction.stressLevel == 1)
					assert.equal(interaction.theme, 'primary')
			})
		})

		it('Should have source object', () => {
			journey.addInteraction({
				date: new Date("2019-05-15 13:33:22"),
				description: "SMS de cobrança. Fatura em aberto",
				stressLevel: 0,
				direction: "out",
				channel: "sms",
				source: {
					id: "lemondesk",
					label: "Lemon Desk",
					icon: "img/lemondesk.png"
				}
			})

			journey.interactions.forEach(interaction => {
				assert.isObject(interaction.source)
			})
		})

		it('Should sort the interactions by dates', () => {
			journey.interactions.forEach(interaction => {
				let next = interaction.next,
					prev = interaction.prev

				if (next)
					assert.isAtMost(interaction.date, next.date)

				if (prev)
					assert.isAtLeast(interaction.date, prev.date)
			})
		})

		it('Should have prev interaction', () => {
			assert.isObject(journey.interactions[1].prev)
		})

		it('Should have next interaction', () => {
			assert.isObject(journey.interactions[0].next)
		})

		it('Should not have prev on the first interaction', () => {
			assert.isNotObject(journey.interactions[0].prev)
		})

		it('Should not have next on the last interaction', () => {
			assert.isNotObject(journey.interactions[journey.interactions.length - 1].next)
		})

		it('Should have a date object', () => {
			journey.interactions.forEach(interaction => {
				assert.typeOf(interaction.date, 'Date')
			})
		})

		it('Should have channel object', () => {
			journey.interactions.forEach(interaction => {
				assert.isObject(interaction.channel)
			})
		})

		it('Should throw error when channel does not exist', () => {
			expect(() => {
				journey.addInteraction({
					date: new Date("2019-05-15 13:33:23"),
					description: "SMS de cobrança. Fatura em aberto",
					stressLevel: 0,
					direction: "out",
					channel: "ps4",
					source: {
						id: "lemondesk",
						label: "Lemon Desk",
						icon: "img/lemondesk.png"
					}
				})
			}).to.throw()
		})
	})

	describe('Events', () => {
		it('Should fire init event', () => {})
		it('Should fire update event', () => {})
		it('Should fire destroy event', () => {})
		it('Should fire addChannel event', () => {})
		it('Should fire addInteraction event', () => {})
		it('Should fire addSource event', () => {})
	})
})
