<div class="cj-journey {{#if active }} is-active {{/if }}">
	<header class="cj-journey__header">
		<span class="cj-journey__title cj-text cj-text--lead">
			Jornada do Cliente
		</span>
		{{> cj-icon
			class="cj-journey__arrow"
			name="fas fa-angle-down"
			theme="default"
		}}
	</header>

	<div class="cj-journey__body">
		{{> cj-select
			class="cj-journey__filter"
			label="Filtrar"
			options=filters
			selected=selectedFilter
		}}

		{{> cj-timeline
			class="cj-journey__timeline"
			interactions=interactions
		}}
	</div>
</div>
