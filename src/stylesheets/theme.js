module.exports = {
	colors: {
		default: '#3b3f44',
		primary: '#46ba54',
    secondary: '#f3922c',
		accent: '#ffb651',
		warn: '#e9294a'
	}
}
