const express = require('express')
const serveStatic = require('serve-static')
const compression = require('compression')
const path = require('path')
const app = express()
app.use(compression())

app.use("/", serveStatic ( path.join (__dirname, '/dist') ) )

const port = process.env.PORT || 5000

app.listen(port)

app.get('*', function (req, res) {
	res.sendFile(__dirname + '/dist/index.html')
})
