# Citrus Journey
Componente que cria uma representa��o visual da jornada do cliente.

## Demonstra��o
Uma demonstra��o do componente est� dispon�vel atrav�s do link: http://citrus-journey.herokuapp.com

## Inicializando
Siga os passos abaixo para inicializar o componente.

### Pr�-requisitos
Clone este resposit�rio em sua aplica��o e importe os arquivos de estilo e scripts:

```html
<link href="./dist/css/journey.min.css" rel="stylesheet">
<script src="./dist/js/journey.min.js" type="text/javascript"></script>
```

#### Font Awesome
Certifique-se que os estilos do Font Awesome est�o presentes no cabe�alho de sua aplica��o, assim os �cones ser�o exibidos corretamente.

```html
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
```

Para inicializar o componente, basta utilizar a classe `Journey`:

### Par�metros
- `element` **{HTMLElement}** Elemento DOM onde ser� utilizado o componente.
- `data` **{Object}** Objeto completo de entrada.
- `options` **{Object}** (opcional) Objeto contendo op��es.

### Op��es
- `active` **{String}** Indica se o corpo do componente deve estar aberto.
- `baseUrl` **{String}** Pr�fixo da localiza��o do diret�rio dos assets.
- `descriptionLength` **{Number}** N�mero limite de caracteres para as descri��es.
- `filters` **{Array}** Array de op��es.
- `selectedFilter` **{Number|String}** Op��o selecionada por padr�o no filtro.
- `stressIcons` **{Object}** Objeto com is �cones representados pelo level de stress.

### Exemplo
```javascript
const data = { ... } // Objeto de entrada
const journey = new Journey(
  document.querySelector("#journey"),
  data,
  {
		active: true,
    baseUrl: "/path/to/your/application/",
		descriptionLength: 60,
    filters: [
      { label: "M�s atual", value: 0 },
      { label: "M�s anterior", value: 1 }
    ],
    selectedFilter: 1,
    stressIcons: {
      "-1": "rage",
      "0": "meh",
      "1": "love"
    }
  }
)
```

## ES05+
Aplica��es que utilizam **ES05+**, � poss�vel importar a biblioteca da seguinte maneira:

```javascript
import Journey from './citrus-journey' // Caminho relativo deste reposit�rio
```

## Depend�ncias
 - Handlebars
 - Font Awesome

# M�todos
`Journey` disponibiliza diversos m�todos para consumo:

## open
Mostra o corpo com toda a jornada do cliente.

### Exemplo
```javascript
journey.open()
```

## close
Esconde o corpo com toda a jornada do cliente.

### Exemplo
```javascript
journey.close()
```

## set
Recebe os dados de entrada e atualiza o estado do componente.

### Par�metros
- `data` **{Object}** Objeto completo de entrada.

### Exemplo
```javascript
journey.set(data)
```

## setFilters
Modifica os filtros em tempo de execu��o.

### Par�metros
- `filters` **{Array}** Array de op��es do filtro.

### Exemplo
```
journey.setFilters([
  {
    label: "�ltimos 90 dias",
    value: 90
  }
])
```

## setChannels
Modifica o objeto de canais em tempo de execu��o.

### Par�metros
- `channels` **{Array}** Array de objetos de entrada do tipo canal.

### Exemplo
```
journey.setChannels([
  {
    "id": "whatsapp",
    "name": "Whatsapp",
    "icon": "img/whatsapp.png"
  },
  ...
])
```

## setInteractions
Modifica as intera��es e atualiza o estado do componente.

### Par�metros
- `interactions` **{Array}** Array de objetos de entrada do tipo intera��o.

### Exemplo
```javascript
journey.setInteractions([
  {
    "id": "fe41c1c4-79b8-11e9-8f9e-2a86e4085a59",
    "date": "2019-05-21T19:33:22",
    "description": "SMS de cobran�a. Fatura em aberto",
    "stressLevel": 0,
    "direction": "out",
    "channel": "sms",
    "source": {
      "id": "limesales",
      "label": "Lime Sales",
      "icon": "img/limesales.png"
    }
  }
])
```

## on
Subscreve um callback em um evento.

### Par�metros
- `name` **{String}** Nome do evento.
- `callback` **{Function}** Fun��o que ser� executada ao evento disparar.

### Exemplo
```javascript
journey.on("selected", interaction => {
  console.log("Interaction selected", interaction)
})
```

## emit
Emite um evento customizado.

### Par�metros
- `name` **{String}** Nome do evento.
- `payload` **{Any}** Objeto que ser� enviado como payload ao evento.
- `target` **HTMLElement** (opcional) Elemento DOM que ir� disparar o evento.

### Exemplo
```javascript
journey.emit("custom-event", { message: "Evento customizado." })
```

## addInteraction
Adiciona uma nova intera��o, retornando um objeto formatado de intera��o.

### Par�metros
- `interaction` **{Object}** Objeto de entrada do tipo intera��o.

### Exemplo
```javascript
journey.addInteraction({
  id: "fe41c1c4-79b8-11e9-8f9e-2a86e4085a59",
  date: new Date("2019-05-15 13:33:22"),
  description: "SMS de cobran�a. Fatura em aberto",
  stressLevel: 0,
  direction: "out",
  channel: "sms",
  source: {
    id: "lemondesk",
    label: "Lemon Desk",
    icon: "img/lemondesk.png"
  }
})
```

## getInteraction
Retorna a intera��o com o id passado.

### Par�metros
- `id` **{String|Number}** C�digo identificador da intera��o.
 
### Exemplo

```javascript
journey.getInteraction("de41c1c4-79b8-11e9-8f9e-2a86e4085a59")
```

# Eventos
## init
Dispara quando o componente � inicializado.

### Payload
- `journey` **{Journey}** Instancia criada.

## destroy
Dispara quando o componente � destruido.

## open
Dispara quando o corpo � aberto.

## close
Dispara quando o corpo � aberto.

## update
Dispara quando o componente atualiza seu estado.

## selected
Dispara quando uma intera��o � selecionada.

## scroll
Dispara quando uma das setas � clicada para rolagem.

## scroll-end
Dispara quando o scroll alcan�a o fim.

## scroll-start
Dispara quando o scroll alcan�a o inicio.

### Payload
- `interaction` **{Object}** Objeto do tipo intera��o.

## filter
Dispara quando o filtro � modificado.

### Payload
- `value` **{Number|String}** Valor selecionado.

## addInteraction
Dispara quando uma intera��o � adicionada.

### Payload
- `interaction` **{Object}** Objeto do tipo intera��o.

# Building
Para compilar a biblioteca utilize o comando `yarn run build` ou `npm run build`.

Os arquivos gerados ir�o ser escritos no diret�rio `/dist`.

# Testes
Executando testes:
`yarn run test` ou `npm run test`

Executando em modo watch:
`yarn run test:watch` ou `npm run test:watch`

# Desenvolvendo
Instale as depend�ncias utilizando `yarn install` ou `npm install`.

Logo em seguida utilize `yarn start` ou `yarn start` para inicializar o servidor de desenvolvimento.

## Criando componentes
TODO

## Usando componentes
TODO

## Objetos de entradas

### Source
```javascript
{
  "id": "limesales",
  "label": "Lime Sales",
  "icon": "img/limesales.png"
}
```

### Channel
```javascript
{
  "id": "chat",
  "name": "Webchat",
  "icon": "img/webchat.png"
},
```

### Interaction
```javascript
{
  "id": "ge41c1c4-79b8-11e9-8f9e-2a86e4085a59",
  "date": "2019-05-15T13:33:22",
  "description": "SMS de cobran�a. Fatura em aberto",
  "stressLevel": -1,
  "direction": "out",
  "channel": "sms",
  "source": {
    "id": "citricbrain",
    "label": "Citric Brain",
    "icon": "img/citricbrain.png"
  }
},
```

# Known issues

## Data mal formatada
Por algum motivo, o navegador Safari apresentou problemas ao renderizar as datas em um formato adequado, quando a data da intera��o recebido pelo json n�o cont�m o `T` de **timezone**, entre a data e hora.

Exemplo:
```javascript
// Retorna NaN/NaN/NaN �s NaN:NaN
{
  "date": "2019-05-15 13:33:22"
  ...
}

// Retorna 05/12/2019 �s 05:00
{
  "date": "2019-05-15T13:33:22"
  ...
}
```
