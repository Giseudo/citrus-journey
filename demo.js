import Journey from './src/journey'
import data from './src/data.mock.json'
import './src/stylesheets/dev.scss'

// Initialize Journey
const journey = new Journey( document.querySelector('#journey'), data, {
	baseUrl: './',
})

// Playground
function randomDate(start, end) {
	return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()))
}

// Add random button
document.querySelector('.cj-app__random')
	.addEventListener('click', event => {
		journey.addInteraction({
			id: Math.floor(Math.random() * 1000),
			date: randomDate(
				new Date("2019-03-01"),
				new Date("2019-04-01")
			),
			description: "SMS de cobrança. Fatura em aberto",
			stressLevel: Math.floor(Math.random() * 3) -1,
			direction: "out",
			channel: "sms",
			source: {
				id: "lemondesk",
				label: "Lemon Desk",
				icon: "img/lemondesk.png"
			}
		})
	})

// On filter change
journey.on('filter', value => {
	console.log('Selected filter', value)

	// Update the entire data
	journey.set(data)
})

// On interaction select
journey.on('selected', interaction => {
	console.log('Selected interaction', interaction)

	alert(interaction.description)
})


journey.on('scroll', direction => {
	console.log('clicked ' + direction)
})

journey.on('scroll-end', direction => {
	console.log('reached end')
})

journey.on('scroll-start', direction => {
	console.log('reached start')
})
